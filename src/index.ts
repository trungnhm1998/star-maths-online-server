
import express from 'express';
import http from 'http';
import WebSocket from 'ws';

import * as jsdom from 'jsdom';
import ApiService from './services/api-services';

function main() {
  let apiService = ApiService.getInstance();
  console.log(apiService);
  let html = `
  <!DOCTYPE html>
  <html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
  </head>
  <body>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.15/lodash.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
    <script src="https://serverjs.starmathsonline.com.au/socket.io/socket.io.js"></script>
    <script src="https://serverjs.starmathsonline.com.au/app.js"></script>
  </body>
  </html>
  `
  let domOptions: jsdom.ConstructorOptions = {
    resources: 'usable',
    runScripts: 'dangerously',
    url: 'http://localhost:8000'
  }

  let dom = new jsdom.JSDOM(html, domOptions);
  dom.window.document.addEventListener('DOMContentLoaded', () => {
    console.log('dom finished loaded')
    let temp: any = dom;
    // console.log(temp.window.app.api.login)
    apiService.registerApi('login', temp.window.app.api.login);
  })
  // setTimeout(() => {
    
  //   console.log(dom.window.app)
  // }, 5000);
  // dom.window.app.api.login({
  //   username: 'anh.cao1',
  //   password: '123456'
  // }).then((accessToken: string) => {
  //   console.log(`accessToken: [${accessToken}]`)
  // })


  const app = express();

  //initialize a simple http server
  const server = http.createServer(app);

  //initialize the WebSocket server instance
  const wss = new WebSocket.Server({ server });

  wss.on('connection', (ws: WebSocket) => {

    //connection is up, let's add a simple simple event
    ws.on('message', (message: string) => {

      //log the received message and send it back to the client
      console.log('received: %s', message);
      ws.send(`Hello, you sent -> ${message}`);
    });

    apiService.callApi('login', {
      username: 'anh.cao1',
      password: '123456'
    }).then((accessToken: any) => {
      //send immediatly a feedback to the incoming connection    
      ws.send('Hi there, I am a WebSocket server ' + accessToken);
    })

  });

  //start our server
  server.listen(process.env.PORT || 8999, () => {
    console.log(`Server started on ${JSON.stringify(server.address())} :)`);
  });
}

main();