export default class ApiService {
  _apiDictionary: { [apiKey: string]: Function } = {};
  
  constructor() {
  }

  static _instance: ApiService;

  static getInstance(): ApiService {
    if (!ApiService._instance) {
      ApiService._instance = new ApiService();
    }
    return ApiService._instance;
  }


  registerApi(key: string, callback: Function) {
    this._apiDictionary[key] = callback;
    console.log(`Registered api [${key}]`);
  }

  callApi(key: string, params = {}): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      const callBack = this._apiDictionary[key];
      callBack(params).then((responseData: Object) => {
        resolve(responseData);
      }).catch((err: Object) => {
        reject(err);
      })
    })
  }
}