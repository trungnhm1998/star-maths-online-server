"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const jsdom = __importStar(require("jsdom"));
function main() {
    jsdom.JSDOM.fromFile('./assets/index.html', {
        resources: 'usable',
        runScripts: 'dangerously',
        url: 'http://localhost/'
    }).then((dom) => {
        console.log('done');
        setTimeout(() => {
            console.log(dom.window.app.api);
            if (dom.window.app.api) {
                dom.window.app.api.login({
                    username: 'anh.cao1',
                    password: '123456'
                }).then((accessToken) => {
                    console.log(`accessToken: [${accessToken}]`);
                });
            }
        }, 1000);
    });
}
main();
//# sourceMappingURL=index.js.map